## Description

12mo (say the "twelve-month" method) is a retrospective method for estimating annual demographic parameters in ruminants livestock herds. It focuses on small-holder farming systems. During farmers interviews, the enumerator’s role is to enumerate exhaustively the animals present in the herds at the date of survey, and then record all the demographic events (births, natural deaths, slaughtering, loans, purchases, etc.) that have occurred in the last twelve months.

12mo was initially designed  to be used in a purely cross-sectional way, with only one shot of interviews. In such a case, the method only provides data for the twelve-month period before the survey (although reproduction rates smoothed over the life of females can also be calculated, with a regression between age and parity of the females). This can be useful, for instance, when the study objective is to evaluate impacts of human interventions (e.g. heath and feeding farming practices or genetic improvements) or of shocks (e.g. disease outbreak, drought or economic crises) on the short term. Nevertheless, 12MO can also be used in a longitudinal way (and this is recommended when feasible), by repeating annually the surveys on the same sample or area. In such a case, data capture the between-years variability and can also be aggregated to evaluate average herd productivities over longer periods. .

A limitation of 12mo is that it can only be used on herds of small or medium size. For practical feasibility reasons, the method does not apply to herds of several hundred animals.

The 12mo method consists of the following resources:

- [Handbook](https://gitlab.cirad.fr/selmet/livtools/12mo/-/blob/master/doc/manual_12mo_v2.2.pdf)
- [Field questionnaires](https://gitlab.cirad.fr/selmet/livtools/12mo/-/tree/master/Quest)
- [Field guide](https://gitlab.cirad.fr/selmet/livtools/12mo/-/tree/master/doc)
- [Survey data collection database](https://gitlab.cirad.fr/selmet/livtools/12mo/-/blob/master/database/12mo_v2.7.accdb)
- [R package t12mo](https://gitlab.cirad.fr/selmet/livtools/t12mo)

## Bibliography

We highly recommend to have a look to the handbook listed below describing the general theory behind calculation of the demographic parameters, in particular concepts such as probability, hazard rate, demographic interferences or competing risks:

[Lesnoff, M., Lancelot, R., Moulin, C.-H., Messad, S., Juanès, X., Sahut, C., 2014](http://link.springer.com/book/10.1007/978-94-017-9026-0). Calculation of demographic parameters in tropical livestock herds: a discrete time approach with laser animal-based monitoring data. Springer, Dordrecht.


## Support

Please send a message to the maintainer: Julia Vuattoux <julia.vuattoux@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)
